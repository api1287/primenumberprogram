class Prime{
	public void checkPrime(int n){
		if(n <= 1){
			System.out.println(n + " is Not Prime");
			return;
		}
		for(int i = 2; i*i < n; i++){
			if(n%i == 0){
				System.out.println(n + " is Not Prime");
				return;
			}
		}
		System.out.println(n + " is Prime");
	}
	public static void main(String [] args){
		Prime obj = new Prime();
		System.out.println("Checking for Prime");
		obj.checkPrime(1);
	}
}
		